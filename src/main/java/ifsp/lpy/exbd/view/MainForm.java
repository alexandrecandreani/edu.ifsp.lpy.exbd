/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifsp.lpy.exbd.view;


import ifsp.lpy.exbd.control.ProdutoService;
import ifsp.lpy.exbd.model.Produto;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import java.awt.Container;

/**
 *
 * @author andreani
 */
public class MainForm  extends JFrame{

    private final ProdutoService produtoService;
    
    public MainForm(final ProdutoService produtoService) {
       this.setTitle("Exemplo");
       
       this.produtoService =   produtoService ;
//       GridLayout experimentLayout = new GridLayout(0,2);
       
    JLabel l=new JLabel("rótulo");
    final JTextField  txt=new JTextField();
		txt.setText("");
                txt.setPreferredSize(new Dimension(100,30));
//                txt.setMaximumSize(new Dimension(100,30));

        JButton button= new JButton();
        button.setText("enviar");
        
        JButton receberButton= new JButton();
        receberButton.setText("receber");
        
    button.addActionListener( new ActionListener(){
        
        public void actionPerformed( ActionEvent e){ 

            System.out.print("botão clicado\n");
        
        //Criar um objeto compatível com o parâmetro do servico         
        Produto produto=new Produto();
           
        //pegar o valor do campo de texto
        produto.setNome(txt.getText());
                

        //atribuir/chamar o método do servico
        produtoService.adicionar(produto);
        
        } } );
    
        receberButton.addActionListener( new ActionListener(){
        
        public void actionPerformed( ActionEvent e){ 

            System.out.print("botão clicado\n");
            List<Produto> produtos= produtoService.buscar_todos();
            
            if (!produtos.isEmpty()){
                produtos.get(0).getNome();

            }
            
            
        
        } } );
    


        
    Container contentPane = this.getContentPane();
    SpringLayout layout=new SpringLayout();
    
    contentPane.setLayout(layout);
    
    contentPane.add(l); 
    contentPane.add(txt);
//    contentPane.add(button);
//    contentPane.add(receberButton);

    JLabel label = l;
    JTextField textField = txt;
 
        layout.putConstraint(SpringLayout.WEST, label,
                             5,
                             SpringLayout.WEST, contentPane);
        
        layout.putConstraint(SpringLayout.NORTH, label,
                             5,
                             SpringLayout.NORTH, contentPane);

 
//        
        layout.putConstraint(SpringLayout.NORTH, textField,
                             5,
                             SpringLayout.NORTH, contentPane);

 
        layout.putConstraint(SpringLayout.EAST, contentPane,
                             5,
                             SpringLayout.EAST, textField);
        
        layout.putConstraint(SpringLayout.WEST, textField,
                             5,
                             SpringLayout.EAST, label);
//        
//        layout.putConstraint(SpringLayout.SOUTH, contentPane,
//                             5,
//                             SpringLayout.SOUTH, textField);
     
     
    this.pack();
    this.setSize(400,400); 
    this.setVisible(true);
    
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    
    
    
    
}
