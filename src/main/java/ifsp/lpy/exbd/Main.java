/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */ 
package ifsp.lpy.exbd;

import ifsp.lpy.exbd.control.ProdutoService;
import ifsp.lpy.exbd.model.Produto;
import ifsp.lpy.exbd.view.MainForm;

/**
 *
 * @author andreani
 */
public class Main {
    
    public static int main(String args[]){
    
        Produto produto = new Produto();
        ProdutoService produtoService = new ProdutoService();
        
        
//            //Schedule a job for the event-dispatching thread:
//        //creating and showing this application's GUI.
//        javax.swing.SwingUtilities.invokeLater(new Runnable() {
//            public void run() {
//           
//            }
//        });
        MainForm    mainForm=new MainForm(produtoService);
        
        
        return 0;
    }
}
