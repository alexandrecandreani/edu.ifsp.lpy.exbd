/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ifsp.lpy.exbd.control;

import ifsp.lpy.exbd.model.Produto; 
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author andreani
 */
public class ProdutoService {
   
    EntityManagerFactory emf =null; 
    EntityManager em=null;
    public ProdutoService(){
        emf = Persistence.createEntityManagerFactory("vendasPU");
        
    }
    
    
    public void adicionar(Produto produto){
        em=emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(produto);
        em.getTransaction().commit();
        em.close();

    }
    
    public List<Produto> buscar_todos(){
        em=emf.createEntityManager();
        List<Produto> produtos  = 
                em.createQuery("SELECT p FROM Produto p").getResultList();
        
        return produtos;
    }
     
    
}
